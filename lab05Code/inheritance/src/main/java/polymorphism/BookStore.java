package polymorphism;

public class BookStore {
    public static void main(String [] args){
        Book[] b = new Book[5];
        b[0] = new Book("wonder woman", "DC");
        b[2] = new Book("My little pony", "Sasha");
        b[1] = new ElectronicBook("Simpsons", "Bob", 25);
        b[3] = new ElectronicBook("Harry Potter", "Finch", 40);
        b[4] = new ElectronicBook("Gaming", "Amy", 30);


        for (int i =0; i < b.length; i++ ){
            System.out.println(b[i].toString());
        }
    }
}

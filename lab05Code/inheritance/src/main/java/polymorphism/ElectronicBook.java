package polymorphism;

public class ElectronicBook extends Book {
    private int numberBytes;


    public ElectronicBook(String title, String author, int numberBytes){
        super(title,author);
        this.numberBytes = numberBytes;
    }

    public int getNumberOfBytes(){
        return this.numberBytes;
    }

    public String toString() {
        String fromBase = super.toString();
        return fromBase + ", " + numberBytes;
    }
}
